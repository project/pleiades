<?php
namespace Drupal\pleiades\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Pleiades controller.
 */
class PleiadesController extends ControllerBase {

  /**
   * Redirects the user to the Pleiades Place node given an pleiades id.
   *
   * Attempts to load the Pleiades Place for the given pleiades id and
   * redirects the user. If no place is found a NotFoundHttpException is
   * thrown (which should throw a 404). If multiple matches are found (and
   * that should not happen) then the first match is used.
   *
   * @param string $pleiades_id
   *  The pleiades id of the place.
   *
   * {@inheritdoc}
   */
  public function place($pleiades_id = NULL) {
    $place_id = NULL;
    if ($pleiades_id) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'pleiades_place')
        ->condition('field_pleiades_id', $pleiades_id);
      $places = $query->execute();
      if (!empty($places)) {
        $place_id = current($places);
      }
    }
    if ($place_id) {
      $response = new RedirectResponse(Url::fromUri("entity:node/{$place_id}", ['absolute' => true])->toString(), 302);
      $response->send();
    }

    // If we've got here then we've not been able to find a nid for a place, or
    // load the node. Just return a 404.
    throw new NotFoundHttpException();

  }

}
