<?php

namespace Drupal\pleiades\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;

/**
 * Parses Pleiades JSON file.
 *
 * This class acts as a base class for any classes which need to access the
 * Pleiades JSON. This file is huge (over 30,000 complex objects in a file >
 * 250MB in size). Parsing using json_decode, even when decoding to associative
 * arrays soon eat up available RAM. This acts as a naieve event-based parser
 * which stores all of the objects in the '@graph' array in the static $data
 * array.
 */
abstract class PleiadesJsonSource extends SourcePluginBase {

  protected $data = array();

  // Naieve cacheing.
  // @todo - replace with something sophisticated
  protected function loadJSON() {
    $cache_file = '/tmp/pcache.txt';
    if (file_exists($cache_file)) {
      $this->data = unserialize(file_get_contents($cache_file));
      \Drupal::logger('pleiades')->notice("Retrieved Pleiades data from log");
    }
    else {
      \Drupal::logger('pleiades')->notice("Attempting to load Pleiades data from file");
      $this->loadJSONFromFile();
      if (!empty($this->data)) {
        file_put_contentS($cache_file, serialize($this->data));
      }
    }
    return (!empty($this->data));
  }

  /**
   * Streaming JSON parser to load data from file.
   *
   * Loads data from a file called pleiades-places-latest.json in this module's
   * /data/ directory.
   *
   * @return boolean
   *   TRUE if we have data, FALSE if not.
   */
  protected function loadJSONFromFile() {
    $f = fopen(drupal_get_path('module', 'pleiades') . '/data/pleiades-places-latest.json', 'r');
    // Spin past the rows until we hit the first item
    $line = 0;
    while (!is_string(strstr(fgets($f),'"@graph": [{'))) {
      $line++;
    };

    // We've stripped off the opening curly brace as it's on the first line, so add it here
    $json_fragment = "{";
    $data = array();
    while ($l = fgets($f)) {
      if (strpos($l, "},")===0) {
        $json_fragment .= "}";
        if ($object = json_decode($json_fragment,TRUE)) {
          $this->data[] = $object;
        }
        else {
          \Drupal::logger('pleiades')->notice("There was an error decoding a json fragment.");
          return false;
        }
        $json_fragment = "";
      }
      else {
        $json_fragment .= $l;
      }
    }
    fclose($f);
  }
}
