<?php

namespace Drupal\pleiades\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Row;

/**
 * Source plugin for pleiades content.
 *
 * @MigrateSource(
 *   id = "pleiades_location_source"
 * )
 */
class PleiadesLocationSource extends PleiadesJSONSource {

  /**
   * Initializes the iterator with the source data.
   *
   * @return array
   *   An array of the data for this source.
   */
  protected function initializeIterator() {
    $data = array();
    if ($this->loadJSON()) {
      foreach ($this->data as $place) {
        foreach ($place["locations"] as $location) {
          $new_location = array(
            'id' => $location["id"],
            'pleiades_id' => $location["uri"],
            'title' => $location["title"],
            'description' => $location["description"],
          );
          if (isset($location["geometry"]["type"]) &&
              $location["geometry"]["type"] == "Point" &&
              isset($location["geometry"]["coordinates"])) {
             $new_location['lon'] = $location["geometry"]["coordinates"][0]; 
             $new_location['lat'] = $location["geometry"]["coordinates"][1]; 
          }
          $data[] = $new_location;
        }
      }
    }
    return new \ArrayIterator($data);
  }

  public function fields() {
    return array('id', 'title','description','pleiades_id');
  }

  public function getIds() {
    return array('id' => ['type' => 'string']);
  }

  public function __toString() {
    return "__CLASS__";
  }

}
