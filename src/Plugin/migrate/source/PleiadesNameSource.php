<?php

namespace Drupal\pleiades\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Row;

/**
 * Source plugin for pleiades content.
 *
 * @MigrateSource(
 *   id = "pleiades_name_source"
 * )
 */
class PleiadesNameSource extends PleiadesJSONSource {

  /**
   * Initializes the iterator with the source data.
   *
   * @return array
   *   An array of the data for this source.
   */
  protected function initializeIterator() {
    $data = array();
    if ($this->loadJSON()) {
      foreach ($this->data as $place) {
        foreach ($place["names"] as $name) {
          $data[] = array(
            'id' => $name["uri"],
            'pleiades_id' => $name["uri"],
            'title' => $name["id"],
            'description' => $name["description"],
          );
        }
      }
    }
    return new \ArrayIterator($data);
  }

  public function fields() {
    return array('id', 'title','description','pleiades_id');
  }

  public function getIds() {
    return array('id' => ['type' => 'string']);
  }

  public function __toString() {
    return "__CLASS__";
  }

}
