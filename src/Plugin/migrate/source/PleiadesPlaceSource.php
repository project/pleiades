<?php

namespace Drupal\pleiades\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Row;

/**
 * Source plugin for pleiades content.
 *
 * @MigrateSource(
 *   id = "pleiades_place_source"
 * )
 */
class PleiadesPlaceSource extends PleiadesJSONSource {

  /**
   * Initializes the iterator with the source data.
   *
   * @return array
   *   An array of the data for this source.
   */
  protected function initializeIterator() {
    $data = array();
    if ($this->loadJSON()) {
      foreach ($this->data as $place) {
        $new_place = array(
          'id' => $place["id"],
          'pleiades_id' => $place["id"],
          'title' => $place["title"],
          'description' => $place["description"],
          'names' => $place["names"],
          'locations' => $place["locations"],
          'lat' => NULL,
          'lon' => NULL,
        );

        // Add the representative point, if there is one
        if (isset($place["reprPoint"])) {
          $new_place['lon'] = $place["reprPoint"][0];
          $new_place['lat'] = $place["reprPoint"][1];
        }

        $data[] = $new_place;
      }
    }
    return new \ArrayIterator($data);
  }

  /**
   * Returns an array of location entity ids for this place.
   *
   * @param array $location
   *  A location from the Pleiades json source.
   */
  static public function getLocations($location) {
    $location_nids = array();

    if (!empty($location['uri'])) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'pleiades_location')
        ->condition('field_pleiades_id', $location['uri']);
      $location_nids = $query->execute();
    }

    return count($location_nids) ? current($location_nids) : NULL;
  }

  /**
   * Returns an array of name entity ids for this place.
   *
   * @param array $name
   *  A name from the Pleiades json source.
   */
  static public function getNames($name) {
    $name_nids = array();

    if (!empty($name['uri'])) {
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'pleiades_name')
        ->condition('field_pleiades_id', $name['uri']);
      $name_nids = $query->execute();
    }
    
    return count($name_nids) ? current($name_nids) : NULL;
  }

  public function fields() {
    return array('id', 'title','description','names','locations','pleiades_id','representative_point');
  }

  public function getIds() {
    return array('id' => ['type' => 'string']);
  }

  public function __toString() {
    return "__CLASS__";
  }

}
